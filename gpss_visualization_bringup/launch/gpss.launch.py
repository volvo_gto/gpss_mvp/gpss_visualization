import os
import xacro
from launch import LaunchDescription
from launch.actions import DeclareLaunchArgument
from launch_ros.actions import Node
from launch_ros.substitutions import FindPackageShare

def generate_launch_description():

    # provide absolute path to the scenario directory
    # TODO: add scenario path as launch argument
    # TODO: add which atrs to launch as launch argument
    frames_path = "/home/gpss/sw/gpss/src/gpss_scenario/frames/"
    rviz_config_file = "/home/gpss/sw/gpss/src/gpss_scenario/config/gpss.rviz"
    atr_description_path = FindPackageShare("atr_description").find("atr_description")
    default_model_path = atr_description_path + "/urdf/atr_robot_base.urdf.xacro"

    ld = LaunchDescription()
    
    if os.path.isdir(frames_path):
        parameters = {"scenario_path": frames_path}

        sms_node = Node(
            package="scene_manipulation_service",
            executable="sms",
            namespace="",
            output="screen",
            parameters=[parameters],
            remappings=[("/tf", "tf"), ("/tf_static", "tf_static")],
            emulate_tty=True,
        )
        ld.add_action(sms_node)

        sms_extra = Node(
            package="scene_manipulation_service",
            executable="sms_extra",
            namespace="",
            output="screen",
            parameters=[parameters],
            remappings=[("/tf", "tf"), ("/tf_static", "tf_static")],
            emulate_tty=True,
        )
        ld.add_action(sms_extra)

        rviz_node = Node(
            package="rviz2",
            executable="rviz2",
            namespace="",
            output="screen",
            arguments=["-d", rviz_config_file],
            emulate_tty=True,
        )
        ld.add_action(rviz_node)

        sms_gui_node = Node(
            package="scene_manipulation_gui",
            executable="scene_manipulation_gui",
            namespace="",
            output="screen",
            parameters=[parameters],
            remappings=[("/tf", "tf"), ("/tf_static", "tf_static")],
            emulate_tty=True,
        )
        ld.add_action(sms_gui_node)

        sms_marker_node = Node(
            package="scene_manipulation_marker",
            executable="scene_manipulation_marker",
            namespace="",
            output="screen",
            parameters=[parameters],
            remappings=[("/tf", "tf"), ("/tf_static", "tf_static")],
            emulate_tty=True,
        )
        ld.add_action(sms_marker_node)

        gpss_visualization_node = Node(
            package="gpss_visualization",
            executable="gpss_visualization",
            namespace="",
            output="screen",
            parameters=[parameters],
            remappings=[("/tf", "tf"), ("/tf_static", "tf_static")],
            emulate_tty=True,
        )
        ld.add_action(gpss_visualization_node)

        for atr_id in [10, 12, 16]:

            print("Launching ATR: " + str(atr_id))

            atr_prefix = "atr_" + str(atr_id) + "_"

            doc = xacro.process_file(default_model_path, mappings={"prefix": atr_prefix, "type": "automower_base"})

            robot_descr_name = "robot_description_" + str(atr_id)
            joint_states_name = "joint_states_" + str(atr_id)
            robot_sate_pub = "robot_state_publisher_" + str(atr_id)

            robot_description = doc.toprettyxml(indent="  ")

            robot_state_publisher_node = Node(
                package="robot_state_publisher",
                executable="robot_state_publisher",
                name=robot_sate_pub,
                parameters=[{"robot_description": robot_description}],
                remappings=[("robot_description", robot_descr_name), ("joint_states", joint_states_name)],
            )
            ld.add_action(robot_state_publisher_node)

        return ld
    else:
        print("Failed, " + frames_path + " is not a valid path.")
        return LaunchDescription()