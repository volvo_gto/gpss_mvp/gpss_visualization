use delaunator::{triangulate, Point as DelaunayPoint};
use earcutr::{self, earcut};
use futures::{stream::Stream, StreamExt};
use nalgebra::{DMatrix, Matrix, Matrix3x4, Matrix4};
use r2r::atr_object_msgs::msg::{ObjectListStamped, ObjectStampedList};
use r2r::builtin_interfaces::msg::{Duration, Time};
use r2r::geometry_msgs::msg::{
    Point, Point32, Polygon, PolygonStamped, Pose, Quaternion, Transform, TransformStamped, Vector3,
};
use r2r::gpss_control_interfaces::msg::ControllerState;
use r2r::scene_manipulation_msgs::srv::GetAllTransforms;
use r2r::std_msgs::msg::{ColorRGBA, Header};
use r2r::tf2_msgs::msg::TFMessage;
use r2r::visualization_msgs::msg::{Marker, MarkerArray};
use r2r::QosProfile;
use serde::{Deserialize, Serialize};
use std::collections::HashMap;
use std::default;
use std::error::Error;
use std::fmt::format;
use std::hash::Hash;
use std::sync::{Arc, Mutex};

pub static NODE_ID: &'static str = "gpss_visualization";
pub static OBSTACLES_MARKER_PUBLISH_RATE: u64 = 200; //ms
pub static OBSTACLES_TF_PUBLISH_RATE: u64 = 200; //ms
pub static AR_TAG_MARKER_PUBLISH_RATE: u64 = 200; //ms
pub static TAG_BROADCAST_RATE: u64 = 200; //ms
pub static AR_TAG_MAINTAIN_RATE:u64 = 200; //ms
pub static AR_TAG_LIFETIME: u64 = 3; //s
pub static BEAMS_MARKER_PUBLISH_RATE: u64 = 5000; //ms
pub static BEAMS_TF_PUBLISH_RATE: u64 = 5000; //ms
pub static EXTRA_FRAME_BROADCAST_RATE: u64 = 100; //ms
pub static CONTROLLER_STATE_PUBLISH_RATE: u64 = 200; //ms
pub static ALL_TRANSFORMS_UPDATE_RATE: u64 = 1000; // ms
pub static AGV_OBSTACLES_MARKER_PUBLISH_RATE: u64 = 200; //ms


// for now we store everything, we might want to use the options later
#[derive(Debug, Default, Serialize, Deserialize, Clone, PartialEq)]
pub struct ObjectData {
    pub header: Header,
    pub polygon: Polygon,
    pub centroid: Point32,
    pub pose: Pose,
    pub parent_stamp: Time,
    pub time_stamp: Time,
    pub class: i8,
    pub type_: i8,
    pub id: i16,
    pub idx: i16,
}

#[tokio::main]
async fn main() -> Result<(), Box<dyn Error>> {
    // setup the node
    let ctx = r2r::Context::create()?;
    let mut node = r2r::Node::create(ctx, NODE_ID, "")?;

    println!("{:?}", camera_frustum_marker_publisher());

    // subscribe to obstacle messages
    let obstacle_listener =
        node.subscribe::<ObjectListStamped>("/area_0/global_objects_fused", QosProfile::default())?;

    // subscribe to atr obstacle messages
    let atr_obstacle_listener =
        node.subscribe::<ObjectListStamped>("/area_0/atr_zones", QosProfile::default())?;

    // subscribe to ar tags
    let ar_tag_listener = node.subscribe::<ObjectStampedList>(
        "/area_0/global_ar_tags_fused",
        r2r::QosProfile::default(),
    )?;

    // subscribe to controller 10 state
    let controller_10_state_listener =
        node.subscribe::<ControllerState>("/atr_10/controller_state", r2r::QosProfile::default())?;

    // subscribe to controller 12 state
    let controller_12_state_listener =
        node.subscribe::<ControllerState>("/atr_12/controller_state", r2r::QosProfile::default())?;

    // subscribe to controller 16 state
    let controller_16_state_listener =
        node.subscribe::<ControllerState>("/atr_16/controller_state", r2r::QosProfile::default())?;

    // publish the controller text marker
    let controller_state_marker_publisher =
        node.create_publisher::<MarkerArray>("controller_state_markers", QosProfile::default())?;

    // publish the planned path as marker
    let planned_path_marker_publisher =
        node.create_publisher::<MarkerArray>("planned_path_markers", QosProfile::default())?;

    let get_all_transforms_client =
        node.create_client::<GetAllTransforms::Service>("/get_all_transforms")?;
    let waiting_for_get_all_transforms_server = node.is_available(&get_all_transforms_client)?;

    // publish the convex obstacle markers to be vizualized
    let convex_obstacle_marker_publisher = node
        .create_publisher::<MarkerArray>("gpss_convex_obstacle_markers", QosProfile::default())?;

    // publish the concave obstacle markers to be vizualized
    let concave_obstacle_marker_publisher = node
        .create_publisher::<MarkerArray>("gpss_concave_obstacle_markers", QosProfile::default())?;

    // publish the convex agv markers to be vizualized
    let convex_agv_marker_publisher =
        node.create_publisher::<MarkerArray>("gpss_convex_agv_markers", QosProfile::default())?;

    // publish the concave agv markers to be vizualized
    let concave_agv_marker_publisher =
        node.create_publisher::<MarkerArray>("gpss_concave_agv_markers", QosProfile::default())?;

    // publish the concave obstacle markers to be vizualized
    let ar_tag_marker_publisher =
        node.create_publisher::<MarkerArray>("ar_tag_markers", QosProfile::default())?;

    // publish beams and walls
    let factory_beams_publisher =
        node.create_publisher::<MarkerArray>("gpss_factory_beams_markers", QosProfile::default())?;

    // publish the active frames to tf (for example detected AR tags)
    // let extra_tf_frame_timer =
    //     node.create_wall_timer(std::time::Duration::from_millis(EXTRA_FRAME_BROADCAST_RATE))?;
    // let extra_tf_frame_broadcaster = node
    //     .create_publisher::<TFExtra>("tf_extra", QosProfile::transient_local(QosProfile::default()))?;

    // TODO: need a task to maintain the controllers states map, i.e. to remove old states so that they are not viz'd
    let obstacles = Arc::new(Mutex::new(Vec::<ObjectData>::new()));
    let agvs = Arc::new(Mutex::new(HashMap::<i16, ObjectData>::new()));
    let ar_tags = Arc::new(Mutex::new(HashMap::<i16, ObjectData>::new()));
    let controller_states = Arc::new(Mutex::new(HashMap::<String, ControllerState>::new()));
    let all_transforms = Arc::new(Mutex::new(HashMap::<String, TransformStamped>::new()));

    // publish the tag frames to tf
    // spawn a tokio task to handle publishing active frames
    let tag_pub_timer =
        node.create_wall_timer(std::time::Duration::from_millis(TAG_BROADCAST_RATE))?;
    let tag_frame_broadcaster = node
        .create_publisher::<TFMessage>("tf", QosProfile::transient_local(QosProfile::default()))?;
    let ar_tags_clone = ar_tags.clone();
    tokio::task::spawn(async move {
        match tag_frame_broadcaster_callback(tag_frame_broadcaster, tag_pub_timer, &ar_tags_clone)
            .await
        {
            Ok(()) => (),
            Err(e) => r2r::log_error!(NODE_ID, "Active frame broadcaster failed with: '{}'.", e),
        };
    });

    // spawn a tokio task to listen to the atr 10 controller state and publish it as a text marker
    for (atr, listener) in [
        ("10", controller_10_state_listener),
        ("12", controller_12_state_listener),
        ("16", controller_16_state_listener),
    ] {
        let controller_states_clone = controller_states.clone();
        let atr_id = atr.to_string();
        tokio::task::spawn(async move {
            match controller_listener_callback(listener, atr_id.clone(), &controller_states_clone)
                .await
            {
                Ok(()) => (),
                Err(e) => r2r::log_error!(
                    NODE_ID,
                    "Controller {} state listener failed with: '{}'.",
                    atr_id,
                    e
                ),
            };
        });
    }

    // spawn a tokio task to listen to objects
    let obstacles_clone_1 = obstacles.clone();
    tokio::task::spawn(async move {
        match object_listener_callback(obstacle_listener, &obstacles_clone_1).await {
            Ok(()) => (),
            Err(e) => r2r::log_error!(NODE_ID, "Obstacle listener failed with: '{}'.", e),
        };
    });

    let agvs_clone = agvs.clone();
    tokio::task::spawn(async move {
        match atr_object_listener_callback(atr_obstacle_listener, &agvs_clone).await {
            Ok(()) => (),
            Err(e) => r2r::log_error!(NODE_ID, "Obstacle listener failed with: '{}'.", e),
        };
    });

    // spawn a tokio task to listen to ar tags
    let ar_tags_clone_1 = ar_tags.clone();
    tokio::task::spawn(async move {
        match ar_tag_listener_callback(ar_tag_listener, &ar_tags_clone_1).await {
            Ok(()) => (),
            Err(e) => r2r::log_error!(NODE_ID, "AR tag listener failed with: '{}'.", e),
        };
    });

    // spawn a tokio task to maintain the tf buffer by tags removing stale tags
    let ar_tag_maintain_timer =
        node.create_wall_timer(std::time::Duration::from_millis(AR_TAG_MAINTAIN_RATE))?;
        let ar_tags_clone = ar_tags.clone();
    tokio::task::spawn(async move {
        match maintain_ar_tags(ar_tag_maintain_timer, &ar_tags_clone).await {
            Ok(()) => (),
            Err(e) => r2r::log_error!(NODE_ID, "AR tag maintainer failed with: '{}'.", e),
        };
    });

    // spawn a tokio task to publish obstacle markers
    let obstacle_marker_timer = node.create_wall_timer(std::time::Duration::from_millis(
        OBSTACLES_MARKER_PUBLISH_RATE,
    ))?;
    let obstacles_clone_2 = obstacles.clone();
    tokio::task::spawn(async move {
        match obstacle_marker_publisher_callback(
            convex_obstacle_marker_publisher,
            concave_obstacle_marker_publisher,
            &obstacles_clone_2,
            obstacle_marker_timer,
        )
        .await
        {
            Ok(()) => println!("done."),
            Err(e) => println!("error: {}", e),
        };
    });

    // spawn a tokio task to publish obstacle markers
    let agv_obstacle_marker_timer = node.create_wall_timer(std::time::Duration::from_millis(
        AGV_OBSTACLES_MARKER_PUBLISH_RATE,
    ))?;
    let agvs_clone = agvs.clone();
    tokio::task::spawn(async move {
        match agv_obstacle_marker_publisher_callback(
            convex_agv_marker_publisher,
            concave_agv_marker_publisher,
            &agvs_clone,
            agv_obstacle_marker_timer,
        )
        .await
        {
            Ok(()) => println!("done."),
            Err(e) => println!("error: {}", e),
        };
    });

    // spawn a tokio task to publish ar tag markers
    let ar_tag_marker_timer =
        node.create_wall_timer(std::time::Duration::from_millis(AR_TAG_MARKER_PUBLISH_RATE))?;
    let ar_tags_clone_2 = ar_tags.clone();
    tokio::task::spawn(async move {
        match ar_tag_marker_publisher_callback(
            ar_tag_marker_publisher,
            &ar_tags_clone_2,
            ar_tag_marker_timer,
        )
        .await
        {
            Ok(()) => println!("done."),
            Err(e) => println!("error: {}", e),
        };
    });

    // spawn a tokio task to publish ar controller state (and maybe planned paths) markers
    let controller_state_marker_timer = node.create_wall_timer(
        std::time::Duration::from_millis(CONTROLLER_STATE_PUBLISH_RATE),
    )?;
    let controller_states_clone = controller_states.clone();
    let all_transforms_clone = all_transforms.clone();
    tokio::task::spawn(async move {
        match controller_state_marker_publisher_callback(
            controller_state_marker_publisher,
            planned_path_marker_publisher,
            &controller_states_clone,
            &all_transforms_clone,
            controller_state_marker_timer,
        )
        .await
        {
            Ok(()) => println!("done."),
            Err(e) => println!("error: {}", e),
        };
    });

    // spawn a tokio task to publish factory beams and walls
    let beams_marker_timer =
        node.create_wall_timer(std::time::Duration::from_millis(BEAMS_MARKER_PUBLISH_RATE))?;
    tokio::task::spawn(async move {
        match factory_beams_publisher_callback(factory_beams_publisher, beams_marker_timer).await {
            Ok(()) => println!("done."),
            Err(e) => println!("error: {}", e),
        };
    });

    let get_all_extras_timer =
        node.create_wall_timer(std::time::Duration::from_millis(ALL_TRANSFORMS_UPDATE_RATE))?;

    // keep the node alive
    let handle = std::thread::spawn(move || loop {
        node.spin_once(std::time::Duration::from_millis(1000));
    });

    r2r::log_warn!(NODE_ID, "Waiting for the Get All Transforms Service...");
    waiting_for_get_all_transforms_server.await?;
    r2r::log_info!(NODE_ID, "Get All Transforms Service available.");

    let all_transforms_clone = all_transforms.clone();
    tokio::task::spawn(async move {
        let result = get_all_transforms(
            get_all_transforms_client,
            &all_transforms_clone,
            get_all_extras_timer,
        )
        .await;
        match result {
            Ok(()) => r2r::log_info!(NODE_ID, "Get All Transforms Service call succeeded."),
            Err(e) => r2r::log_error!(
                NODE_ID,
                "Get All Transforms Service call failed with: {}.",
                e
            ),
        };
    });

    r2r::log_warn!(NODE_ID, "Node started.");

    handle.join().unwrap();

    Ok(())
}

// regular Delaunay algorithm to get triangles
fn triangulate_obstacle_as_convex(polygon: Polygon) -> Vec<Point> {
    let delaunay_points: Vec<DelaunayPoint> = polygon
        .points
        .iter()
        .map(|p| DelaunayPoint {
            x: p.x as f64,
            y: p.y as f64,
        })
        .collect();
    let mut result = triangulate(&delaunay_points).triangles;
    let mut triangles = vec![];
    loop {
        match result.len() >= 3 {
            true => {
                let triange_refs = result.split_off(3);
                let triangle: Vec<Point> = result
                    .iter()
                    .map(|idx| &delaunay_points[idx.to_owned()])
                    .map(|p| Point {
                        x: p.x as f64,
                        y: p.y as f64,
                        z: 0.0,
                    })
                    .collect();
                result = triange_refs;
                triangles.extend(triangle);
            }
            false => break,
        }
    }
    triangles
}

fn triangulate_obstacle_as_concave(polygon: Polygon) -> Vec<Point> {
    let mut point_list = vec![];
    let mut point_list_tuples = vec![];
    polygon.points.iter().for_each(|p| {
        point_list.push(p.x as f64);
        point_list.push(p.y as f64);
    });
    polygon
        .points
        .iter()
        .for_each(|p| point_list_tuples.push((p.x as f64, p.y as f64)));
    let mut result = earcut(&point_list, &vec![], 2);
    let mut triangles = vec![];
    loop {
        match result.len() >= 3 {
            true => {
                let triange_refs = result.split_off(3);
                let triangle_point_list: Vec<(f64, f64)> = result
                    .iter()
                    .map(|idx| point_list_tuples[idx.to_owned()])
                    .collect();
                let triangle = vec![
                    Point {
                        x: triangle_point_list[0].0,
                        y: triangle_point_list[0].1,
                        z: 0.0,
                    },
                    Point {
                        x: triangle_point_list[1].0,
                        y: triangle_point_list[1].1,
                        z: 0.0,
                    },
                    Point {
                        x: triangle_point_list[2].0,
                        y: triangle_point_list[2].1,
                        z: 0.0,
                    },
                ];
                result = triange_refs;
                triangles.extend(triangle);
            }
            false => break,
        }
    }
    triangles
}

// task to remove all stale active frames older than ACTIVE_FRAME_LIFETIME
async fn maintain_ar_tags(
    mut timer: r2r::Timer,
    ar_tags: &Arc<Mutex<HashMap<i16, ObjectData>>>
) -> Result<(), Box<dyn std::error::Error>> {
    loop {
        let tags_local = ar_tags.lock().unwrap().clone();
        let mut tags_local_reduced = tags_local.clone();
        let mut clock = r2r::Clock::create(r2r::ClockType::RosTime).unwrap();
        let now = clock.get_now().unwrap();
        let current_time = r2r::Clock::to_builtin_time(&now);
        
        tags_local.iter().for_each(|(k, v)| {
            let stamp = v.clone().time_stamp;
            match current_time.sec > stamp.sec + AR_TAG_LIFETIME as i32 {
                true => {
                    tags_local_reduced.remove(k);
                }
                false => () // do nothing if the tag is fresh
            }
        });
        *ar_tags.lock().unwrap() = tags_local_reduced;
        timer.tick().await?;
    }
}

// updates the object buffer with fresh object data
async fn object_listener_callback(
    mut subscriber: impl Stream<Item = ObjectListStamped> + Unpin,
    obstacles: &Arc<Mutex<Vec<ObjectData>>>,
) -> Result<(), Box<dyn std::error::Error>> {
    let mut clock = r2r::Clock::create(r2r::ClockType::RosTime).unwrap();
    loop {
        let now = clock.get_now().unwrap();
        let time_stamp = r2r::Clock::to_builtin_time(&now);
        match subscriber.next().await {
            Some(message) => {
                let mut obstacles_local = vec![];
                message
                    .objects
                    .iter()
                    .for_each(|obj| match obj.object_c.o_class {
                        7 => {}
                        _ => obstacles_local.push(ObjectData {
                            header: message.header.clone(),
                            polygon: obj.polygon.clone(),
                            centroid: obj.centroid.clone(),
                            pose: obj.pose.clone(),
                            parent_stamp: message.parent_stamp.clone(),
                            time_stamp: Time { sec: 0, nanosec: 0 },
                            class: obj.object_c.o_class,
                            type_: obj.object_t.o_type,
                            id: obj.object_id,
                            idx: obj.object_idx,
                        }),
                    });
                *obstacles.lock().unwrap() = obstacles_local;
            }
            None => {
                r2r::log_error!(NODE_ID, "Object subscriber did not get the message?");
            }
        }
    }
}

async fn atr_object_listener_callback(
    mut subscriber: impl Stream<Item = ObjectListStamped> + Unpin,
    agvs: &Arc<Mutex<HashMap<i16, ObjectData>>>,
) -> Result<(), Box<dyn std::error::Error>> {
    let mut clock = r2r::Clock::create(r2r::ClockType::RosTime).unwrap();
    loop {
        let now = clock.get_now().unwrap();
        let time_stamp = r2r::Clock::to_builtin_time(&now);
        match subscriber.next().await {
            Some(message) => {
                let mut agvs_local = agvs.lock().unwrap().clone();
                message
                    .objects
                    .iter()
                    .for_each(|obj| match obj.object_c.o_class {
                        7 => {
                            agvs_local.insert(
                                obj.object_id,
                                ObjectData {
                                    header: message.header.clone(),
                                    polygon: obj.polygon.clone(),
                                    centroid: obj.centroid.clone(),
                                    pose: obj.pose.clone(),
                                    parent_stamp: message.parent_stamp.clone(),
                                    time_stamp: time_stamp.clone(),
                                    class: obj.object_c.o_class,
                                    type_: obj.object_t.o_type,
                                    id: obj.object_id,
                                    idx: obj.object_idx,
                                },
                            );
                        }
                        _ => {}
                    });
                *agvs.lock().unwrap() = agvs_local;
            }
            None => {
                r2r::log_error!(NODE_ID, "Object subscriber did not get the message?");
            }
        }
    }
}

// updates the ar tag buffer with fresh ar tag data
async fn controller_listener_callback(
    mut subscriber: impl Stream<Item = ControllerState> + Unpin,
    atr_id: String,
    controller_states: &Arc<Mutex<HashMap<String, ControllerState>>>,
) -> Result<(), Box<dyn std::error::Error>> {
    loop {
        match subscriber.next().await {
            Some(message) => {
                let mut controller_states_local = controller_states.lock().unwrap().clone();
                controller_states_local.insert(atr_id.clone(), message);
                *controller_states.lock().unwrap() = controller_states_local;
            }
            None => {
                r2r::log_error!(
                    NODE_ID,
                    "Controller {} state subscriber did not get the message?",
                    atr_id
                );
            }
        }
    }
}

// updates the ar tag buffer with fresh ar tag data
async fn ar_tag_listener_callback(
    mut subscriber: impl Stream<Item = ObjectStampedList> + Unpin,
    ar_tags: &Arc<Mutex<HashMap<i16, ObjectData>>>,
) -> Result<(), Box<dyn std::error::Error>> {
    loop {
        match subscriber.next().await {
            Some(message) => {
                let mut ar_tags_local = ar_tags.lock().unwrap().clone();
                let mut clock = r2r::Clock::create(r2r::ClockType::RosTime).unwrap();
                let now = clock.get_now().unwrap();
                let time_stamp = r2r::Clock::to_builtin_time(&now);
                message.objects.iter().for_each(|obj| {
                    ar_tags_local.insert(obj.object.object_id, ObjectData {
                        header: message.header.clone(),
                        // polygon: obj.object.polygon.clone(),
                        polygon: Polygon {
                            // fake the ar_tag size until we get the actual sizes
                            points: vec![
                                Point32 {
                                    x: 0.0,
                                    y: 0.0,
                                    z: 0.0,
                                },
                                Point32 {
                                    x: 0.2,
                                    y: 0.0,
                                    z: 0.0,
                                },
                                Point32 {
                                    x: 0.2,
                                    y: 0.2,
                                    z: 0.0,
                                },
                                Point32 {
                                    x: 0.0,
                                    y: 0.2,
                                    z: 0.0,
                                },
                                Point32 {
                                    x: 0.0,
                                    y: 0.0,
                                    z: 0.0,
                                },
                            ],
                        },
                        centroid: obj.object.centroid.clone(),
                        pose: obj.object.pose.clone(),
                        parent_stamp: Time { sec: 0, nanosec: 0 }, // what the hell is parent stamp?
                        time_stamp: time_stamp.clone(),
                        class: obj.object.object_c.o_class,
                        type_: obj.object.object_t.o_type,
                        id: obj.object.object_id,
                        idx: obj.object.object_idx,
                    });
                });
                *ar_tags.lock().unwrap() = ar_tags_local;
            }
            None => {
                r2r::log_error!(NODE_ID, "AR tag subscriber did not get the message?");
            }
        }
    }
}

// async fn extra_tf_frame_broadcaster_callback(
//     publisher: r2r::Publisher<TFExtra>,
//     mut timer: r2r::Timer,
//     ar_tags: &Arc<Mutex<Vec<ObjectData>>>,
// ) -> Result<(), Box<dyn std::error::Error>> {
//     loop {
//         let mut clock = r2r::Clock::create(r2r::ClockType::RosTime).unwrap();
//         let now = clock.get_now().unwrap();
//         let time_stamp = r2r::Clock::to_builtin_time(&now);

//         let ar_tags_local = frames.lock().unwrap().clone();
//         let mut ar_transforms = vec![];

//         ar_tags_local.iter().for_each(||)

//         transforms_local.iter().for_each(|(_, v)| match v.active {
//             true => {
//                 updated_transforms.push(TransformStamped {
//                     header: Header {
//                         stamp: time_stamp.clone(),
//                         frame_id: v.parent_frame_id.clone(),
//                     },
//                     child_frame_id: v.child_frame_id.clone(),
//                     transform: v.transform.clone(),
//                 });
//             }
//             false => (),
//         });

//         let msg = TFMessage {
//             transforms: updated_transforms,
//         };

//         match publisher.publish(&msg) {
//             Ok(()) => (),
//             Err(e) => {
//                 r2r::log_error!(
//                     NODE_ID,
//                     "Active broadcaster failed to send a message with: '{}'",
//                     e
//                 );
//             }
//         };
//         timer.tick().await?;
//     }
// }



// TODO: should be general, all objects should be in a hashmap and then we can filter what we want
async fn tag_frame_broadcaster_callback(
    publisher: r2r::Publisher<TFMessage>,
    mut timer: r2r::Timer,
    tags: &Arc<Mutex<HashMap<i16, ObjectData>>>,
) -> Result<(), Box<dyn std::error::Error>> {
    loop {
        let mut clock = r2r::Clock::create(r2r::ClockType::RosTime).unwrap();
        let now = clock.get_now().unwrap();
        let time_stamp = r2r::Clock::to_builtin_time(&now);

        let tags_local = tags.lock().unwrap().clone();
        let mut transforms = vec![];

        tags_local.iter().for_each(|tag| {
            let q = quaternion::euler_angles(
                tag.1.pose.orientation.x,
                tag.1.pose.orientation.y,
                tag.1.pose.orientation.z,
            );
            transforms.push(TransformStamped {
                header: Header {
                    stamp: time_stamp.clone(),
                    frame_id: "world".to_string(),
                },
                child_frame_id: format!("tag_{}", tag.1.id),
                transform: Transform {
                    translation: Vector3 {
                        x: tag.1.pose.position.x,
                        y: tag.1.pose.position.y,
                        z: tag.1.pose.position.z,
                    },
                    rotation: Quaternion {
                        x: q.1[0],
                        y: q.1[1],
                        z: q.1[2],
                        w: q.0,
                    },
                },
            });
        });

        let msg = TFMessage {
            transforms,
        };

        match publisher.publish(&msg) {
            Ok(()) => (),
            Err(e) => {
                r2r::log_error!(
                    NODE_ID,
                    "Tag broadcaster failed to send a message with: '{}'",
                    e
                );
            }
        };
        timer.tick().await?;
    }
}

async fn controller_state_marker_publisher_callback(
    controller_state_publisher: r2r::Publisher<MarkerArray>,
    planned_path_publisher: r2r::Publisher<MarkerArray>,
    controller_states: &Arc<Mutex<HashMap<String, ControllerState>>>,
    transforms: &Arc<Mutex<HashMap<String, TransformStamped>>>,
    mut timer: r2r::Timer,
) -> Result<(), Box<dyn std::error::Error>> {
    let mut clock = r2r::Clock::create(r2r::ClockType::RosTime).unwrap();
    loop {
        let mut controller_state_markers: Vec<Marker> = vec![];
        let mut planned_path_markers: Vec<Marker> = vec![];
        let controller_states_local = controller_states.lock().unwrap().clone();
        let transforms_local = transforms.lock().unwrap().clone();

        let mut id = 0;
        let now = clock.get_now().unwrap();
        let time_stamp = r2r::Clock::to_builtin_time(&now);
        for controller in controller_states_local {
            id = id + 1;
            let indiv_marker = Marker {
                header: Header {
                    stamp: time_stamp.clone(),
                    frame_id: "world".to_string(),
                },
                ns: "".to_string(),
                id,
                type_: 9,
                action: 0,
                color: ColorRGBA {
                    r: 1.0,
                    g: 1.0,
                    b: 1.0,
                    a: 1.0,
                },
                text: format!(
                    "status: {} \nrunning: {} \ntime_to_wp: {} \ntime_to_goal: {} \nnext: {}, \ncompleted: {}",
                    controller.1.status.chars().filter(|c| !c.is_whitespace()).collect::<String>(),
                    controller.1.running,
                    match controller.1.time_to_waypoint.len() > 1 {
                        true => format!("{:.1}", controller.1.time_to_waypoint[1]),
                        false => format!("{}", 0.0)
                    },
                    match controller.1.time_to_waypoint.len() > 1 {
                        true => {
                            let mut to_sum = vec!();
                            for nr in 0..controller.1.time_to_waypoint.len() - 1 {
                                to_sum.push(controller.1.time_to_waypoint[nr+1])
                            }
                            format!("{:.1}", to_sum.iter().sum::<f64>())
                        }
                        false => format!("{}", 0.0)
                    },
                    match controller.1.path_completed {
                        true => "".to_string(),
                        false => controller.1.path[0].clone(),
                    },
                    controller.1.path_completed
                ),
                pose: Pose {
                    position: Point {
                        x: controller.1.pose.position.x, // + 1.7,
                        y: controller.1.pose.position.y, // + 0.0,
                        z: controller.1.pose.position.z - 2.5,
                    },
                    orientation: controller.1.pose.orientation,
                },
                lifetime: Duration { sec: 3, nanosec: 0 },
                scale: Vector3 {
                    x: 0.0,
                    y: 0.0,
                    z: 0.2,
                },
                ..Marker::default()
            };
            controller_state_markers.push(indiv_marker);
            let path_len = controller.1.path.len();
            for step in 0..path_len - 1 {
                match transforms_local.get(&controller.1.path[step]) {
                    Some(begin_in_world) => {
                        match transforms_local.get(&controller.1.path[step + 1]) {
                            Some(end_in_world) => {
                                id = id + 1;
                                let indiv_marker = Marker {
                                    header: Header {
                                        stamp: time_stamp.clone(),
                                        frame_id: "world".to_string(),
                                    },
                                    ns: "".to_string(),
                                    id,
                                    type_: 0,
                                    action: 0,
                                    points: vec![
                                        Point {
                                            x: begin_in_world.transform.translation.x,
                                            y: begin_in_world.transform.translation.y,
                                            z: begin_in_world.transform.translation.z,
                                        },
                                        Point {
                                            x: end_in_world.transform.translation.x,
                                            y: end_in_world.transform.translation.y,
                                            z: end_in_world.transform.translation.z,
                                        },
                                    ],
                                    lifetime: Duration { sec: 2, nanosec: 0 },
                                    scale: Vector3 {
                                        x: 0.1,
                                        y: 0.2,
                                        z: 0.5,
                                    },
                                    color: ColorRGBA {
                                        r: 0.0,
                                        g: 0.0,
                                        b: 255.0,
                                        a: 0.75,
                                    },
                                    ..Marker::default()
                                };
                                planned_path_markers.push(indiv_marker)
                            }
                            None => (),
                        }
                    }
                    None => (),
                }
            }
        }

        let controller_state_array_msg = MarkerArray {
            markers: controller_state_markers,
        };

        let planned_path_array_msg = MarkerArray {
            markers: planned_path_markers,
        };

        match controller_state_publisher.publish(&controller_state_array_msg) {
            Ok(()) => (),
            Err(e) => {
                r2r::log_error!(
                    NODE_ID,
                    "Publisher failed to send controller state marker message with: {}",
                    e
                );
            }
        };

        match planned_path_publisher.publish(&planned_path_array_msg) {
            Ok(()) => (),
            Err(e) => {
                r2r::log_error!(
                    NODE_ID,
                    "Publisher failed to send planned path marker message with: {}",
                    e
                );
            }
        };

        timer.tick().await?;
    }
}

async fn obstacle_marker_publisher_callback(
    _convex_publisher: r2r::Publisher<MarkerArray>,
    concave_publisher: r2r::Publisher<MarkerArray>,
    obstacles: &Arc<Mutex<Vec<ObjectData>>>,
    mut timer: r2r::Timer,
) -> Result<(), Box<dyn std::error::Error>> {
    let mut clock = r2r::Clock::create(r2r::ClockType::RosTime).unwrap();
    loop {
        let mut convex_obstacle_markers: Vec<Marker> = vec![];
        let mut concave_obstacle_markers: Vec<Marker> = vec![];
        let obstacles_local = obstacles.lock().unwrap().clone();

        // test obstacle
        // obstacles_local.push(ObjectData {
        //     header: Header {
        //         stamp: Time { sec: 0, nanosec: 0 },
        //         frame_id: "world".to_string(),
        //     },
        //     pose: Pose {
        //         position: Point {
        //             x: 5.0,
        //             y: 5.0,
        //             z: 0.0,
        //         },
        //         orientation: Quaternion {
        //             x: 0.0,
        //             y: 0.0,
        //             z: 0.0,
        //             w: 1.0,
        //         },
        //     },
        //     obstacle: obstacle {
        //         points: vec![
        //             Point32 {
        //                 x: 0.0,
        //                 y: 0.0,
        //                 z: 0.0,
        //             },
        //             Point32 {
        //                 x: 1.0,
        //                 y: 0.0,
        //                 z: 0.0,
        //             },
        //             Point32 {
        //                 x: 1.0,
        //                 y: 1.0,
        //                 z: 0.0,
        //             },
        //             Point32 {
        //                 x: 2.0,
        //                 y: 1.0,
        //                 z: 0.0,
        //             },
        //             Point32 {
        //                 x: 2.0,
        //                 y: 0.0,
        //                 z: 0.0,
        //             },
        //             Point32 {
        //                 x: 3.0,
        //                 y: 0.0,
        //                 z: 0.0,
        //             },
        //             Point32 {
        //                 x: 3.0,
        //                 y: 3.0,
        //                 z: 0.0,
        //             },
        //             Point32 {
        //                 x: 0.0,
        //                 y: 3.0,
        //                 z: 0.0,
        //             },
        //             Point32 {
        //                 x: 0.0,
        //                 y: 0.0,
        //                 z: 0.0,
        //             },
        //         ],
        //     },
        //     ..Default::default()
        // });

        let mut id = 0;
        let now = clock.get_now().unwrap();
        let time_stamp = r2r::Clock::to_builtin_time(&now);
        for obstacle in obstacles_local.clone() {
            let triangulated_convex = triangulate_obstacle_as_convex(obstacle.polygon);
            if triangulated_convex.len() < 3 {
                continue;
            }
            id = id + 1;
            let indiv_marker = Marker {
                header: Header {
                    stamp: time_stamp.clone(),
                    frame_id: obstacle.header.frame_id.clone(),
                },
                ns: "".to_string(),
                id,
                type_: 11,
                action: 0,
                points: triangulated_convex.clone(),
                colors: triangulated_convex
                    .iter()
                    .map(|_| ColorRGBA {
                        r: 0.4,
                        g: 0.0,
                        b: 0.6,
                        a: 0.2,
                    })
                    .collect(),
                color: ColorRGBA {
                    r: 1.0,
                    g: 0.0,
                    b: 0.0,
                    a: 1.0,
                },
                pose: obstacle.pose,
                lifetime: Duration { sec: 5, nanosec: 0 },
                scale: Vector3 {
                    x: 1.0,
                    y: 1.0,
                    z: 1.0,
                },
                ..Marker::default()
            };
            convex_obstacle_markers.push(indiv_marker)
        }

        for obstacle in obstacles_local {
            let triangulated_concave = triangulate_obstacle_as_concave(obstacle.polygon);
            id = id + 1;
            let indiv_marker = Marker {
                header: Header {
                    stamp: time_stamp.clone(),
                    frame_id: obstacle.header.frame_id.clone(),
                },
                ns: "".to_string(),
                id,
                type_: 11,
                action: 0,
                points: triangulated_concave.clone(),
                colors: triangulated_concave
                    .iter()
                    .map(|_| ColorRGBA {
                        r: 1.0,
                        g: 0.2882,
                        b: 0.2784,
                        a: 1.0,
                    })
                    .collect(),
                color: ColorRGBA {
                    r: 1.0,
                    g: 0.2882,
                    b: 0.2784,
                    a: 1.0,
                },
                pose: obstacle.pose,
                lifetime: Duration { sec: 5, nanosec: 0 },
                scale: Vector3 {
                    x: 1.0,
                    y: 1.0,
                    z: 1.0,
                },
                ..Marker::default()
            };
            concave_obstacle_markers.push(indiv_marker)
        }

        let convex_obstacle_marker_array_msg = MarkerArray {
            markers: convex_obstacle_markers,
        };

        let concave_obstacle_marker_array_msg = MarkerArray {
            markers: concave_obstacle_markers,
        };

        // match convex_publisher.publish(&convex_obstacle_marker_array_msg) {
        //     Ok(()) => (),
        //     Err(e) => {
        //         r2r::log_error!(
        //             NODE_ID,
        //             "Publisher failed to send obstacle marker message with: {}",
        //             e
        //         );
        //     }
        // };

        match concave_publisher.publish(&concave_obstacle_marker_array_msg) {
            Ok(()) => (),
            Err(e) => {
                r2r::log_error!(
                    NODE_ID,
                    "Publisher failed to send obstacle marker message with: {}",
                    e
                );
            }
        };

        timer.tick().await?;
    }
}

async fn agv_obstacle_marker_publisher_callback(
    convex_publisher: r2r::Publisher<MarkerArray>,
    concave_publisher: r2r::Publisher<MarkerArray>,
    obstacles: &Arc<Mutex<HashMap<i16, ObjectData>>>,
    mut timer: r2r::Timer,
) -> Result<(), Box<dyn std::error::Error>> {
    let mut clock = r2r::Clock::create(r2r::ClockType::RosTime).unwrap();
    loop {
        let mut convex_obstacle_markers: Vec<Marker> = vec![];
        let mut concave_obstacle_markers: Vec<Marker> = vec![];
        let obstacles_local = obstacles.lock().unwrap().clone();

        let mut id = 0;
        let now = clock.get_now().unwrap();
        let time_stamp = r2r::Clock::to_builtin_time(&now);
        for obstacle in obstacles_local.clone() {
            let triangulated_convex = triangulate_obstacle_as_convex(obstacle.1.polygon);
            id = id + 1;
            let indiv_marker = Marker {
                header: Header {
                    stamp: time_stamp.clone(),
                    frame_id: obstacle.1.header.frame_id.clone(),
                },
                ns: "".to_string(),
                id,
                type_: 11,
                action: 0,
                points: triangulated_convex.clone(),
                colors: triangulated_convex
                    .iter()
                    .map(|_| ColorRGBA {
                        r: 0.0,
                        g: 0.0,
                        b: 1.0,
                        a: 0.2,
                    })
                    .collect(),
                color: ColorRGBA {
                    r: 0.0,
                    g: 0.0,
                    b: 1.0,
                    a: 1.0,
                },
                pose: obstacle.1.pose,
                lifetime: Duration { sec: 5, nanosec: 0 },
                scale: Vector3 {
                    x: 1.0,
                    y: 1.0,
                    z: 1.0,
                },
                ..Marker::default()
            };
            convex_obstacle_markers.push(indiv_marker)
        }

        for obstacle in obstacles_local {
            let triangulated_concave = triangulate_obstacle_as_concave(obstacle.1.polygon);
            id = id + 1;
            let indiv_marker = Marker {
                header: Header {
                    stamp: time_stamp.clone(),
                    frame_id: obstacle.1.header.frame_id.clone(),
                },
                ns: "".to_string(),
                id,
                type_: 11,
                action: 0,
                points: triangulated_concave.clone(),
                colors: triangulated_concave
                    .iter()
                    .map(|_| ColorRGBA {
                        r: 0.0,
                        g: 0.0,
                        b: 1.0,
                        a: 0.2,
                    })
                    .collect(),
                color: ColorRGBA {
                    r: 0.0,
                    g: 0.0,
                    b: 1.0,
                    a: 1.0,
                },
                pose: obstacle.1.pose,
                lifetime: Duration { sec: 5, nanosec: 0 },
                scale: Vector3 {
                    x: 1.0,
                    y: 1.0,
                    z: 1.0,
                },
                ..Marker::default()
            };
            concave_obstacle_markers.push(indiv_marker)
        }

        let convex_obstacle_marker_array_msg = MarkerArray {
            markers: convex_obstacle_markers,
        };

        let concave_obstacle_marker_array_msg = MarkerArray {
            markers: concave_obstacle_markers,
        };

        match convex_publisher.publish(&convex_obstacle_marker_array_msg) {
            Ok(()) => (),
            Err(e) => {
                r2r::log_error!(
                    NODE_ID,
                    "Publisher failed to send atr obstacle marker message with: {}",
                    e
                );
            }
        };

        match concave_publisher.publish(&concave_obstacle_marker_array_msg) {
            Ok(()) => (),
            Err(e) => {
                r2r::log_error!(
                    NODE_ID,
                    "Publisher failed to send atr obstacle marker message with: {}",
                    e
                );
            }
        };

        timer.tick().await?;
    }
}

async fn ar_tag_marker_publisher_callback(
    publisher: r2r::Publisher<MarkerArray>,
    ar_tags: &Arc<Mutex<HashMap<i16, ObjectData>>>,
    mut timer: r2r::Timer,
) -> Result<(), Box<dyn std::error::Error>> {
    let mut clock = r2r::Clock::create(r2r::ClockType::RosTime).unwrap();
    loop {
        let mut ar_tag_markers: Vec<Marker> = vec![];
        let ar_tags_local = ar_tags.lock().unwrap().clone();

        let mut id = 0;
        let now = clock.get_now().unwrap();
        let time_stamp = r2r::Clock::to_builtin_time(&now);
        for obstacle in ar_tags_local.clone() {
            // println!("POLYGON: {:?}", obstacle.polygon);
            let triangulated_convex = triangulate_obstacle_as_convex(obstacle.1.polygon.clone());
            // println!("TRIANGULATED: {:?}", obstacle.polygon.clone());
            id = id + 1;
            let indiv_marker = Marker {
                header: Header {
                    stamp: time_stamp.clone(),
                    frame_id: obstacle.1.header.frame_id.clone(),
                },
                ns: "".to_string(),
                id,
                type_: 11,
                action: 0,
                points: triangulated_convex.clone(),
                colors: triangulated_convex
                    .iter()
                    .map(|_| ColorRGBA {
                        r: 0.0,
                        g: 0.0,
                        b: 1.0,
                        a: 0.7,
                    })
                    .collect(),
                color: ColorRGBA {
                    r: 0.0,
                    g: 0.0,
                    b: 1.0,
                    a: 1.0,
                },
                pose: obstacle.1.pose,
                lifetime: Duration { sec: 3, nanosec: 0 },
                scale: Vector3 {
                    x: 1.0,
                    y: 1.0,
                    z: 1.0,
                },
                ..Marker::default()
            };
            ar_tag_markers.push(indiv_marker)
        }

        let ar_tag_marker_array_msg = MarkerArray {
            markers: ar_tag_markers,
        };

        match publisher.publish(&ar_tag_marker_array_msg) {
            Ok(()) => (),
            Err(e) => {
                r2r::log_error!(
                    NODE_ID,
                    "Publisher failed to send ar tag marker message with: {}",
                    e
                );
            }
        };

        timer.tick().await?;
    }
}

async fn factory_beams_publisher_callback(
    publisher: r2r::Publisher<MarkerArray>,
    mut timer: r2r::Timer,
) -> Result<(), Box<dyn std::error::Error>> {
    let beam_scale = Vector3 {
        x: 0.25,
        y: 0.25,
        z: 6.0,
    };

    let mut clock = r2r::Clock::create(r2r::ClockType::RosTime).unwrap();
    loop {
        let mut beam_markers: Vec<Marker> = vec![];
        let mut id = 0;
        let now = clock.get_now().unwrap();
        let time_stamp = r2r::Clock::to_builtin_time(&now);
        for beam_nr in 0..10 {
            id = id + 1;
            let indiv_marker = Marker {
                header: Header {
                    stamp: time_stamp.clone(),
                    frame_id: "world".to_string(),
                },
                ns: "".to_string(),
                id,
                type_: 1,
                action: 0,
                color: ColorRGBA {
                    r: 0.3351,
                    g: 0.3391,
                    b: 0.3257,
                    a: 1.0,
                },
                pose: Pose {
                    position: Point {
                        x: (beam_nr as f64) * 6.0,
                        y: 6.3,
                        z: 3.0,
                    },
                    orientation: Quaternion {
                        x: 0.0,
                        y: 0.0,
                        z: 0.0,
                        w: 1.0,
                    },
                },
                lifetime: Duration {
                    sec: 10,
                    nanosec: 0,
                },
                scale: beam_scale.clone(),
                ..Marker::default()
            };
            beam_markers.push(indiv_marker)
        }

        let beam_markers_array_msg = MarkerArray {
            markers: beam_markers,
        };

        match publisher.publish(&beam_markers_array_msg) {
            Ok(()) => (),
            Err(e) => {
                r2r::log_error!(
                    NODE_ID,
                    "Publisher failed to send ar beam marker message with: {}",
                    e
                );
            }
        };

        timer.tick().await?;
    }
}

// TODO: read from files and publish as markers
fn camera_frustum_marker_publisher() -> [r2r::geometry_msgs::msg::Point; 4] {
    let image_shape = [2160., 3840., 3.];

    let test = Matrix3x4::new(
        -2.0601770819447324e+03,
        -1.0096880423845032e+01,
        -1.9502756616212578e+03,
        3.9698547281425323e+04,
        -1.1589556510118372e+01,
        2.0895704078439921e+03,
        -1.0610407003248640e+03,
        -7.0504303414418719e+03,
        1.4752990822483289e-02,
        8.9916341116169311e-03,
        -9.9985073874943686e-01,
        4.5505171660597412e+00,
    );

    let assemble = Matrix4::new(
        test.row(0)[0],
        test.row(0)[1],
        test.row(0)[2],
        test.row(0)[3],
        test.row(1)[0],
        test.row(1)[1],
        test.row(1)[2],
        test.row(1)[3],
        test.row(2)[0],
        test.row(2)[1],
        1.0,
        0.0,
        test.row(2)[0],
        test.row(2)[1],
        test.row(2)[2],
        test.row(2)[3],
    );

    fn pixel_to_3d(
        pixels: [f64; 2],
        assemble: Matrix<
            f64,
            nalgebra::Const<4>,
            nalgebra::Const<4>,
            nalgebra::ArrayStorage<f64, 4, 4>,
        >,
    ) -> Point {
        let transformation_mat = match assemble.try_inverse() {
            Some(x) => x,
            None => panic!(),
        };

        let temp = nalgebra::Matrix4x1::new(pixels[1], pixels[0], 0.0, 1.0);

        let solution = transformation_mat * temp;
        let x_world = solution / solution[3];
        Point {
            x: x_world[0],
            y: x_world[1],
            z: 0.0, // rough estimation instead of projecting x_world[2], but could be done later
        }
    }

    let roi_corner1 = pixel_to_3d([image_shape[0] - 1.0, image_shape[1] - 1.0], assemble);
    let roi_corner2 = pixel_to_3d([0.0, 0.0], assemble);
    let roi_corner3 = pixel_to_3d([0.0, image_shape[1] - 1.0], assemble);
    let roi_corner4 = pixel_to_3d([image_shape[0] - 1.0, 0.0], assemble);

    [roi_corner1, roi_corner3, roi_corner2, roi_corner4]
}

// ask the lookup service for transforms from its buffer
async fn get_all_transforms(
    client: r2r::Client<GetAllTransforms::Service>,
    transforms: &Arc<Mutex<HashMap<String, TransformStamped>>>,
    mut timer: r2r::Timer,
) -> Result<(), Box<dyn std::error::Error>> {
    loop {
        let request = GetAllTransforms::Request {
            parent_frame_id: "world".to_string(),
        };

        let response = client
            .request(&request)
            .expect("Could not send get all transforms request.")
            .await
            .expect("Cancelled.");

        // r2r::log_info!(
        //     NODE_ID,
        //     "Request to get all transforms in frame '{}' sent.",
        //     "world",
        // );

        match response.success {
            true => {
                let to_add = response
                    .transforms
                    .iter()
                    .map(|t| (t.child_frame_id.clone(), t.clone()))
                    .collect();
                *transforms.lock().unwrap() = to_add
            }
            false => {
                r2r::log_error!(NODE_ID, "Couldn't get all transforms.",);
            }
        }
        timer.tick().await?;
    }
}
